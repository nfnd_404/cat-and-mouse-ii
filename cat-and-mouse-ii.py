# https://leetcode.cn/problems/cat-and-mouse-ii/

from collections import deque


class Solution:
    # 全局变量
    grid = []
    H,W = 0,0 # grid的大小
    cat_jump,mouse_jump = 0,0
    mcj = [] # 用于扩展grid，以cat_jump为步幅测量任意两个位置间的距离
    mmj = [] # 用于扩展grid，以mouse_jump为步幅测量任意两个位置间的距离
    count_step = 0 # 总递归调用次数，检查递归次数过大
    mouse_win_map = [] # 记忆鼠先手时不同位置的胜负
    cat_win_map = [] # 记忆猫先手时不同位置的胜负
    mouse_direction = {'u':[],'r':[],'d':[],'l':[],'o':[[0,0]]} # 猫可能走的方向
    cat_direction = {'u':[],'r':[],'d':[],'l':[],'o':[[0,0]]} # 鼠可能走的方向

    def reset(self):
        # 重置所有全局变量
        self.grid.clear()
        self.mouse_win_map.clear()
        self.cat_win_map.clear()
        self.mcj.clear()
        self.mmj.clear()
        self.H,self.W = 0,0
        self.cat_jump,self.mouse_jump = 0,0
        self.count_step = 0
        for d in self.mouse_direction:
            self.mouse_direction[d].clear()
        for d in self.cat_direction:
            self.cat_direction[d].clear()
        self.mouse_direction['o'].append([0,0])
        self.cat_direction['o'].append([0,0])

    def pos_equal(self,p1,p2)->bool:
        # 判断两个位置是否相同
        if p1[0]==p2[0] and p1[1]==p2[1]:
            return True
        else:
            return False
    
    def pos_valid(self,pos)->bool:
        # 判断走到pos位置是否违规，包括出界和撞墙
        res = True
        if 0>pos[0] or pos[0]>=self.H or 0>pos[1] or pos[1]>=self.W:
            res = False
        elif self.grid[pos[0]][pos[1]]=='#':
            res = False
        return res

    def cat_pos_valid(self,mouse_pos,cat_pos,food_pos,new_cat_pos,total_step)->bool:
        # 判断猫走到new_cat_pos位置是否有收益
        res = False
        # 猫先手且位置有优势时只要奔向食物就一定能赢，此时猫的最优选择是明确的唯一种
        if self.mmj[mouse_pos[0]*self.W+mouse_pos[1]][food_pos[0]*self.W+food_pos[1]]>=self.mcj[cat_pos[0]*self.W+cat_pos[1]][food_pos[0]*self.W+food_pos[1]]:
            res = self.mcj[cat_pos[0]*self.W+cat_pos[1]][food_pos[0]*self.W+food_pos[1]]>self.mcj[new_cat_pos[0]*self.W+new_cat_pos[1]][food_pos[0]*self.W+food_pos[1]]
        # 猫没有位置优势时不一定输，此时猫需要堵住鼠的路，即缩小与鼠的距离
        else:
            res = self.mcj[cat_pos[0]*self.W+cat_pos[1]][mouse_pos[0]*self.W+mouse_pos[1]]>=self.mcj[new_cat_pos[0]*self.W+new_cat_pos[1]][mouse_pos[0]*self.W+mouse_pos[1]]
            res = res or (self.mmj[mouse_pos[0]*self.W+mouse_pos[1]][cat_pos[0]*self.W+cat_pos[1]]>=self.mmj[mouse_pos[0]*self.W+mouse_pos[1]][new_cat_pos[0]*self.W+new_cat_pos[1]])
        return res

    def mouse_pos_valid(self,mouse_pos,cat_pos,food_pos,new_mouse_pos,total_step)->bool:
        # 判断老鼠走到new_mouse_pos位置是否有收益
        res = False
        # 鼠先手且位置更靠近食物时也不一定赢，但是鼠失去位置优势就一定会输，所以鼠一方面要保持位置优势，另一方面要博取收益；但需要注意，鼠的位置优势可能很大，这样即使鼠采取回撤策略也没有什么问题
        if self.mmj[new_mouse_pos[0]*self.W+new_mouse_pos[1]][food_pos[0]*self.W+food_pos[1]]<self.mcj[cat_pos[0]*self.W+cat_pos[1]][food_pos[0]*self.W+food_pos[1]]:
            # 鼠有上中下三策，所谓上中下之分，是在一般情况下说的，并不是上策就一定能赢，有时可能下策反而更有效
            # 鼠缩小到食物的距离为上策，不用考虑步数问题，如果能到达食物，1000步之内必到
            res = self.mmj[mouse_pos[0]*self.W+mouse_pos[1]][food_pos[0]*self.W+food_pos[1]]>self.mmj[new_mouse_pos[0]*self.W+new_mouse_pos[1]][food_pos[0]*self.W+food_pos[1]]
            # 鼠不管与食物的距离而闪避猫为中策，当 total_step<1000 时，鼠可以选择回撤闪避；超过这个步数，猫没有到达食物，也没有抓到鼠，那说明猫只能通过拖时间的方式获胜，这时鼠不能再拖时间了；但在1000步内，并不能找到一个限制，说超过这个限制就可以不考虑中策和下策这样，所以在一定步数之后，还是要时不时试一下，也不能总试
            res = res or ((total_step<400 or 3<=total_step%8<6) and self.mcj[cat_pos[0]*self.W+cat_pos[1]][mouse_pos[0]*self.W+mouse_pos[1]]<=self.mcj[cat_pos[0]*self.W+cat_pos[1]][new_mouse_pos[0]*self.W+new_mouse_pos[1]])
            # 鼠只是保持与食物的距离，也不管能不能闪避猫为下策，这个策略也不能一直用，也不能在一定步数后就完全不用，需要考虑步数
            # 下下策是鼠扩大与食物的距离，也不闪避猫，这样没准也能赢，但如果这样能赢，那中策也一定能赢，因为中策同样可能扩大与食物的距离，但需要闪避猫，赢面一定比这个策略大，所以最多考虑到下策，下下策是不予考虑的
            res = res or ((total_step<400 or 3<=total_step%8<6) and self.mmj[mouse_pos[0]*self.W+mouse_pos[1]][food_pos[0]*self.W+food_pos[1]]==self.mmj[new_mouse_pos[0]*self.W+new_mouse_pos[1]][food_pos[0]*self.W+food_pos[1]])
        # 鼠一旦失去位置优势就可以投降了，也不必再躲着猫
        # else:
            
        return res
        
    def mouse_walk(self,mouse_pos,cat_pos,food_pos,total_step)->bool:
        # 该方法代表鼠走到 mouse_pos 的一次操作，注意只要进入该方法(也就是到这一行)就表示鼠已经做出了行动，所以接下来先判断胜负，紧接着就是猫的行动，res一开始设置为1，因为在没有得到猫的反应前鼠认为自己的操作能使自己赢
        mp,cp,fp,ts,res = mouse_pos,cat_pos,food_pos,total_step,1
        self.count_step+=1
        print("\rcount step: "+format(self.count_step,'.7E'),end="")
        # 胜负判断
        if ts>990: # 不能只用布尔表示胜负，到达限制步数其实应该算平局
            return 0
        if self.pos_equal(mp,fp):
            return 1
        # 猫行动开始
        for d,cj in self.cat_direction.items():
            for j in cj:
                if not self.pos_valid([cp[0]+j[0],cp[1]+j[1]]):
                    break
                # 猫快速查表看有没有自己能赢的办法
                if self.mouse_win_map[mp[0]*self.W+mp[1]][(cp[0]+j[0])*self.W+cp[1]+j[1]]<0:
                    res = self.mouse_win_map[mp[0]*self.W+mp[1]][(cp[0]+j[0])*self.W+cp[1]+j[1]]
                    return res
        # 猫查表不能查到，并不代表猫一定没法赢，接下来猫尝试把没走过的走法亲身尝试一遍
        for d,cj in self.cat_direction.items():
            for j in cj:
                if not self.pos_valid([cp[0]+j[0],cp[1]+j[1]]):
                    break
                if self.mouse_win_map[mp[0]*self.W+mp[1]][(cp[0]+j[0])*self.W+cp[1]+j[1]]!=0:
                    # 如果j走法已经行不通了，猫快速过掉，不会去递归浪费时间
                    res = self.mouse_win_map[mp[0]*self.W+mp[1]][(cp[0]+j[0])*self.W+cp[1]+j[1]]
                elif self.cat_pos_valid(mp,cp,fp,[cp[0]+j[0],cp[1]+j[1]],ts+1):
                    # j走法如果确实没有尝试过，猫会调用递归函数，返回该走法的结果
                    res = self.cat_walk(mp,[cp[0]+j[0],cp[1]+j[1]],fp,ts+1)
                    # 返回鼠胜，则猫基于j走法的所有可能都不能使猫获胜，猫想赢就要继续遍历，调整j尝试别的走法
                    # 返回鼠负，则鼠基于当前位置的所有走法都能被猫用j走法破解，鼠想赢只能返回上一步调整位置
                    # 返回平局，则不能确定鼠当前的走法和猫的j走法谁会赢
                    if res!=0:
                        self.mouse_win_map[mp[0]*self.W+mp[1]][(cp[0]+j[0])*self.W+cp[1]+j[1]] = res
                        if res<0:
                            self.cat_win_map[mp[0]*self.W+mp[1]][(cp[0])*self.W+cp[1]] = res                      
                if res<=0:
                    return res
        return res

    def cat_walk(self,mouse_pos,cat_pos,food_pos,total_step):
        # 该方法代表猫走到 cat_pos 的一次操作，注意只要进入该方法(也就是到这一行)就表示猫已经做出了行动，所以接下来先判断胜负，紧接着就是鼠的行动，res一开始设置为-1，因为在没有得到鼠的反应前猫认为自己的操作能使自己赢
        mp,cp,fp,ts,res = mouse_pos,cat_pos,food_pos,total_step,-1
        self.count_step+=1
        print("\rcount step: "+format(self.count_step,'.7E'),end="")
        # 胜负判断
        if ts>990: # 不能只用布尔表示胜负，到达限制步数其实应该算平局
            return 0
        if self.pos_equal(cp,mp):
            return -1
        if self.pos_equal(cp,fp):
            return -1
        # 鼠行动开始
        for d,mj in self.mouse_direction.items():
            for j in mj:
                if not self.pos_valid([mp[0]+j[0],mp[1]+j[1]]):
                    break
                # 鼠快速查表看有没有自己能赢的办法
                if self.cat_win_map[(mp[0]+j[0])*self.W+mp[1]+j[1]][(cp[0])*self.W+cp[1]]>0:
                    res = self.cat_win_map[(mp[0]+j[0])*self.W+mp[1]+j[1]][cp[0]*self.W+cp[1]]
                    return res
        # 鼠查表不能查到，并不代表鼠一定没法赢，接下来鼠尝试把没走过的走法亲身尝试一遍
        for d,mj in self.mouse_direction.items():
            for j in mj:
                if not self.pos_valid([mp[0]+j[0],mp[1]+j[1]]):
                    break
                if self.cat_win_map[(mp[0]+j[0])*self.W+mp[1]+j[1]][(cp[0])*self.W+cp[1]]!=0:
                    # 如果j走法已经行不通了，鼠快速过掉，不会去递归浪费时间
                    res = self.cat_win_map[(mp[0]+j[0])*self.W+mp[1]+j[1]][(cp[0])*self.W+cp[1]]
                elif self.mouse_pos_valid(mp,cp,fp,[mp[0]+j[0],mp[1]+j[1]],ts+1):
                    # j走法如果确实没有尝试过，鼠会调用递归函数，返回该走法的结果
                    res = self.mouse_walk([mp[0]+j[0],mp[1]+j[1]],cp,fp,ts+1)
                    # 返回猫胜，则鼠基于j走法的所有可能都不能使鼠获胜，鼠想赢就要继续循环，尝试别的j
                    # 返回猫负，则猫基于当前位置的所有走法都能被鼠用j走法破解，猫想赢只能返回上一步调整位置
                    # 返回平局，则不能确定鼠当前走法和猫的j走法谁会赢，但鼠需要调整
                    if res!=0:
                        self.cat_win_map[(mp[0]+j[0])*self.W+mp[1]+j[1]][(cp[0])*self.W+cp[1]] = res
                        if res>0:
                            self.mouse_win_map[(mp[0])*self.W+mp[1]][cp[0]*self.W+cp[1]] = res
                if res>0:
                    return res
        return res
        
    
    # use case
    # https://leetcode.cn/problems/cat-and-mouse-ii/solution/yu-le-by-arbitrarily-s6fn/
    def canMouseWin(self, grid, catJump: int, mouseJump: int) -> bool:
        # 游戏开始，重置所有全局变量，然后分别赋传入的值
        self.reset()
        self.cat_jump = catJump
        self.mouse_jump = mouseJump
        self.grid.extend(grid)
        self.H,self.W = len(self.grid),len(self.grid[0])
        self.mouse_win_map.extend([[0]*self.H*self.W for l in range(self.H*self.W)])
        self.cat_win_map.extend([[0]*self.H*self.W for l in range(self.H*self.W)])
        
        for r in range(self.H):
            for c in range(self.W):
                if self.grid[r][c]!='#':
                    if self.grid[r][c]=='C':
                        cat_init_pos = [r,c]
                    elif self.grid[r][c]=='M':
                        mouse_init_pos = [r,c]
                    elif self.grid[r][c]=='F':
                        food_pos = (r,c)
        
            
        for j in range(1,self.mouse_jump+1):
            self.mouse_direction['r'].append([0,j])
            self.mouse_direction['l'].append([0,-j])
            self.mouse_direction['d'].append([j,0])
            self.mouse_direction['u'].append([-j,0])

        for j in range(1,self.cat_jump+1):
            self.cat_direction['r'].append([0,j])
            self.cat_direction['l'].append([0,-j])
            self.cat_direction['d'].append([j,0])
            self.cat_direction['u'].append([-j,0])
        
        # 对grid做扩展，求出任意两点的最短距离
        pos_queue = deque([])
        self.mcj.extend([[self.H*self.W]*self.H*self.W for l in range(self.H*self.W)])
        for r in range(self.H):
            for c in range(self.W):
                if self.grid[r][c]!='#':
                    self.mcj[r*self.W+c][r*self.W+c] = 0
                    pos_queue.append((r,c))
                    while len(pos_queue)>0:
                        pos = pos_queue.popleft()
                        for d,cj in self.cat_direction.items():
                            for j in cj:
                                if not self.pos_valid((pos[0]+j[0],pos[1]+j[1])):
                                    break
                                elif self.mcj[r*self.W+c][(pos[0]+j[0])*self.W+pos[1]+j[1]]>self.mcj[r*self.W+c][pos[0]*self.W+pos[1]]+1:
                                    self.mcj[r*self.W+c][(pos[0]+j[0])*self.W+pos[1]+j[1]] = self.mcj[r*self.W+c][pos[0]*self.W+pos[1]]+1
                                    pos_queue.append((pos[0]+j[0],pos[1]+j[1]))
                        

        self.mmj.extend([[self.H*self.W]*self.H*self.W for l in range(self.H*self.W)])
        for r in range(self.H):
            for c in range(self.W):
                if self.grid[r][c]!='#':
                    self.mmj[r*self.W+c][r*self.W+c] = 0
                    pos_queue.append((r,c))
                    while len(pos_queue)>0:
                        pos = pos_queue.popleft()
                        for d,mj in self.mouse_direction.items():
                            for j in mj:
                                if not self.pos_valid((pos[0]+j[0],pos[1]+j[1])):
                                    break
                                elif self.mmj[r*self.W+c][(pos[0]+j[0])*self.W+pos[1]+j[1]]>self.mmj[r*self.W+c][pos[0]*self.W+pos[1]]+1:
                                    self.mmj[r*self.W+c][(pos[0]+j[0])*self.W+pos[1]+j[1]] = self.mmj[r*self.W+c][pos[0]*self.W+pos[1]]+1
                                    pos_queue.append((pos[0]+j[0],pos[1]+j[1]))
                                

        res = self.cat_walk(mouse_init_pos,cat_init_pos,food_pos,0)>0
        print()
        return res


if __name__=="__main__":
    s = Solution()
    print(s.canMouseWin(grid = ["####F","#C...","M...."] , catJump = 1 , mouseJump = 2))
    